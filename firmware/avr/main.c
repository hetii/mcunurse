#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <inttypes.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "io.h"
#include "xprintf.h"
#include "analog.h"
#include "strtoumax.h"


int main(void){

    DDRA = 0xFF;
    // DDRB = 0xFF;
    DDRB = (0xFF & ~(1<<PB0));
    DDRC = 0xFF;
    DDRD = 0xFF;
    // DDRE = 0xFF;
    DDRE = (0xFF & ~((1<<PE6)|(1<<PE7)));
    DDRF = 0xFF;
    DDRG = 0xFF;

    PORTA = 0x0;
    PORTB = 0x0;
    PORTC = 0x0;
    PORTD = 0x0;
    PORTE = 0x0;
    PORTF = 0x0;
    PORTG = 0x0;

    while(1){
        _delay_ms(500);
        PORTA = 0x0;
        PORTB = 0x0;
        PORTC = 0x0;
        PORTD = 0x0;
        PORTE = 0x0;
        PORTF = 0x0;
        PORTG = 0x0;
        _delay_ms(500);
        PORTA = 0xFF;
        // PORTB = 0xFF;
        PORTB = (0xFF & ~(1<<PB0));
        PORTC = 0xFF;
        PORTD = 0xFF;
        // PORTE = 0xFF;
        PORTE = (0xFF & ~((1<<PE6)|(1<<PE7)));
        PORTF = 0xFF;
        PORTG = 0xFF;
    }
    return 0;
}
