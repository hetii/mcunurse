#ifndef __SSD1306_H__
#define __SSD1306_H__

// #include <libopencm3/stm32/i2c.h>

#include "ssd1306_fonts.h"

#define SSD1306_I2C_BUS I2C1
#define SSD1306_I2C_ADDR 0x3c // (0x3C << 1)
// some LEDs don't display anything in first two columns
// #define SSD1306_WIDTH           130
#define SSD1306_WIDTH 128
#define SSD1306_HEIGHT 64

#define SSD1306_MIRROR_VERT 1
#define SSD1306_MIRROR_HORIZ 1

#define _IF_SB(i2c) ((I2C_SR1(i2c) & I2C_SR1_SB) == 0)
#define _IF_BTF(i2c) ((I2C_SR1(i2c) & I2C_SR1_BTF) == 0)
#define _IF_ADDR(i2c) ((I2C_SR1(i2c) & I2C_SR1_ADDR) == 0)
#define _IF_TxE(i2c) (I2C_SR1(i2c) & I2C_SR1_TxE) == 0

 // Enumeration for screen colors
typedef enum {
    Black = 0x00, // Black color, no pixel
    White = 0x01  // Pixel is set. Color depends on OLED
} SSD1306_COLOR;

// Struct to store transformations
typedef struct {
    uint16_t CurrentX;
    uint16_t CurrentY;
    uint8_t Inverted;
} SSD1306_t;

// Procedure definitions
void ssd1306_on(void);
void ssd1306_off(void);
void ssd1306_init(void);
void ssd1306_fill(SSD1306_COLOR color);
void ssd1306_update_screen(void);
void ssd1306_draw_pixel(uint8_t x, uint8_t y, SSD1306_COLOR color);
// char ssd1306_write_char(char ch, FontDef Font, SSD1306_COLOR color);
char ssd1306_write_string(char* str, FontDef Font, SSD1306_COLOR color);
void ssd1306_set_cursor(uint8_t x, uint8_t y);

extern uint8_t SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8];

#endif // __SSD1306_H__
