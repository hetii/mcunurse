#include <libopencm3/stm32/i2c.h>

#include "ssd1306.h"
#include "platform.h"

// Screenbuffer
#ifndef SSD1306_Buffer
    uint8_t SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8] = {0};
#endif

// Screen object
static SSD1306_t SSD1306;

uint32_t reg32 __attribute__((unused));
static void ssd1306_start(void) {
  i2c_send_start(SSD1306_I2C_BUS);
  while (_IF_SB(SSD1306_I2C_BUS));
  i2c_send_7bit_address(SSD1306_I2C_BUS, SSD1306_I2C_ADDR, I2C_WRITE);
  while (_IF_ADDR(SSD1306_I2C_BUS));
  //Cleaning ADDR condition sequence.
  reg32 = I2C_SR2(SSD1306_I2C_BUS);
}

static void ssd1306_stop(void) {
  i2c_send_stop(SSD1306_I2C_BUS);
  while (_IF_BTF(SSD1306_I2C_BUS));
}

static void ssd1306_send(uint8_t spec) {
  i2c_send_data(SSD1306_I2C_BUS, spec);
  while (_IF_TxE(SSD1306_I2C_BUS));
}

// Send a byte to the command register
static void ssd1306_write_command(uint8_t byte) {
  ssd1306_start();
  ssd1306_send(0x00);
  ssd1306_send(byte);
  ssd1306_stop();
}

// Send data
static void ssd1306_write_data(uint8_t *buffer, uint16_t buff_size) {
  ssd1306_start();
  ssd1306_send(0x40);
  for (uint16_t i=0; i < buff_size; i++) {
    i2c_send_data(SSD1306_I2C_BUS, buffer[i]); //todo make it with DMA later
    while (_IF_TxE(SSD1306_I2C_BUS));
  }
}

// Initialize the oled screen

/*static const uint8_t ssd1306_initialization_values [] = {
    0xAE, 0x20, 0x10, 0xB0, 0xC0, 0x00, 0x10, 0x40, 0x81, 0xFF,
    0xA0, 0xA6, 0xA8, 0x3F, 0xA4, 0xD3, 0x00, 0xD5, 0xF0, 0xD9,
    0x22, 0xDA, 0x12, 0xDB, 0x20, 0x8D, 0x14, 0xAF
};*/
void ssd1306_init(void) {
    /*for (uint8_t i = 0; i < sizeof(ssd1306_initialization_values); ++i)
    {
        ssd1306_write_command(ssd1306_initialization_values[i]);
    }
    ssd1306_update_screen();*/
    ssd1306_write_command(0xAE); //display off

    ssd1306_write_command(0x20); //Set Memory Addressing Mode   
    ssd1306_write_command(0x10); // 00,Horizontal Addressing Mode; 01,Vertical Addressing Mode;
                                 // 10,Page Addressing Mode (RESET); 11,Invalid
    ssd1306_write_command(0xB0); //Set Page Start Address for Page Addressing Mode,0-7

#ifdef SSD1306_MIRROR_VERT
    ssd1306_write_command(0xC0); // Mirror vertically
#else
    ssd1306_write_command(0xC8); //Set COM Output Scan Direction
#endif

    ssd1306_write_command(0x00); //---set low column address
    ssd1306_write_command(0x10); //---set high column address

    ssd1306_write_command(0x40); //--set start line address - CHECK

    ssd1306_write_command(0x81); //--set contrast control register - CHECK
    ssd1306_write_command(0xFF);

#ifdef SSD1306_MIRROR_HORIZ
    ssd1306_write_command(0xA0); // Mirror horizontally
#else
    ssd1306_write_command(0xA1); //--set segment re-map 0 to 127 - CHECK
#endif

#ifdef SSD1306_INVERSE_COLOR
    ssd1306_write_command(0xA7); //--set inverse color
#else
    ssd1306_write_command(0xA6); //--set normal color
#endif

    ssd1306_write_command(0xA8); //--set multiplex ratio(1 to 64) - CHECK
    ssd1306_write_command(0x3F); //

    ssd1306_write_command(0xA4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content

    ssd1306_write_command(0xD3); //-set display offset - CHECK
    ssd1306_write_command(0x00); //-not offset

    ssd1306_write_command(0xD5); //--set display clock divide ratio/oscillator frequency
    ssd1306_write_command(0xF0); //--set divide ratio

    ssd1306_write_command(0xD9); //--set pre-charge period
    ssd1306_write_command(0x22); //

    ssd1306_write_command(0xDA); //--set com pins hardware configuration - CHECK
    ssd1306_write_command(0x12);

    ssd1306_write_command(0xDB); //--set vcomh
    ssd1306_write_command(0x20); //0x20,0.77xVcc

    ssd1306_write_command(0x8D); //--set DC-DC enable
    ssd1306_write_command(0x14); //
    ssd1306_write_command(0xAF); //--turn on SSD1306 panel
    // Clear screen:
    // Flush buffer to screen, buffer here have 0x00
    // so it`s the same with: ssd1306_fill(Black);
    //ssd1306_fill(Black);
    ssd1306_update_screen();
    
    // Set default values for screen object
    SSD1306.CurrentX = 0;
    SSD1306.CurrentY = 0;
}

/*#include <stdio.h>
DEBUG("START:\n");
for (unsigned int i = 0; i < (SSD1306_WIDTH * SSD1306_HEIGHT / 8); ++i)
{
    DEBUG("0x%02X,\n", SSD1306_Buffer[i]);
}
DEBUG("END:\n");*/

void ssd1306_off(void)
{
    ssd1306_write_command(0xAE);
}

void ssd1306_on(void)
{
    ssd1306_write_command(0xAF);
}

// Write the screenbuffer with changed to the screen
void ssd1306_update_screen(void) {
    for(uint8_t i = 0; i < 8; i++) {
        ssd1306_write_command(0xB0 + i);
        ssd1306_write_command(0x00);
        ssd1306_write_command(0x10);
        ssd1306_write_data(&SSD1306_Buffer[SSD1306_WIDTH*i],SSD1306_WIDTH);
    }
}

//#ifndef STRIPPED

// Fill the whole screen with the given color
void ssd1306_fill(SSD1306_COLOR color) {
    /* Set memory */
    for(uint32_t i = 0; i < sizeof(SSD1306_Buffer); i++) {
        SSD1306_Buffer[i] = (color == Black) ? 0x00 : 0xFF;
    }
}

//    Draw one pixel in the screenbuffer
//    X => X Coordinate
//    Y => Y Coordinate
//    color => Pixel color
void ssd1306_draw_pixel(uint8_t x, uint8_t y, SSD1306_COLOR color) {
    if(x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT) {
        // Don't write outside the buffer
        return;
    }
    
    // Check if pixel should be inverted
    if(SSD1306.Inverted) {
        color = (SSD1306_COLOR)!color;
    }
    
    // Draw in the right color
    if(color == White) {
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] |= 1 << (y % 8);
    } else { 
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] &= ~(1 << (y % 8));
    }
}

// Draw 1 char to the screen buffer
// ch         => char om weg te schrijven
// Font     => Font waarmee we gaan schrijven
// color     => Black or White
static char ssd1306_write_char(char ch, FontDef Font, SSD1306_COLOR color) {
    uint32_t i, b, j;
    
    // Check remaining space on current line
    if (SSD1306_WIDTH <= (SSD1306.CurrentX + Font.FontWidth) ||
        SSD1306_HEIGHT <= (SSD1306.CurrentY + Font.FontHeight))
    {
        // Not enough space on current line
        return 0;
    }
    
    // Use the font to write
    for(i = 0; i < Font.FontHeight; i++) {
        b = Font.data[(ch - 32) * Font.FontHeight + i];
        for(j = 0; j < Font.FontWidth; j++) {
            if((b << j) & 0x8000)  {
                ssd1306_draw_pixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), (SSD1306_COLOR) color);
            } else {
                ssd1306_draw_pixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), (SSD1306_COLOR)!color);
            }
        }
    }
    
    // The current space is now taken
    SSD1306.CurrentX += Font.FontWidth;
    
    // Return written char for validation
    return ch;
}

// Write full string to screenbuffer
char ssd1306_write_string(char* str, FontDef Font, SSD1306_COLOR color) {
    // Write until null-byte
    while (*str) {
        if (ssd1306_write_char(*str, Font, color) != *str) {
            // Char could not be written
            return *str;
        }
        // Next char
        str++;
    }
    // Everything ok
    return *str;
}

// Position the cursor
void ssd1306_set_cursor(uint8_t x, uint8_t y) {
    SSD1306.CurrentX = x;
    SSD1306.CurrentY = y;
}
//#endif