#include "general.h"
#include "serprog.h"

void serprog_write(char *buf, int len){
    // DEBUG("Flashrom: serprog_write called %d:\n", len);
    (void)len;
    char tx[64]={0};

    if (buf[0] == S_CMD_SYNCNOP){
    	tx[0] = S_NAK;
    	tx[1] = S_ACK;
    	usbuart_write(tx, 2);

    } else if (buf[0] == S_CMD_NOP){
    	tx[0] = S_ACK;
    	usbuart_write(tx, 1);

    } else if (buf[0] == S_CMD_Q_IFACE){
    	tx[0] = S_ACK;
    	tx[1] = 1;
    	tx[2] = 0;
    	usbuart_write(tx, 3);
    } else if (buf[0] == S_CMD_Q_CMDMAP){
		tx[0] = S_ACK;
    	tx[1] = 1;
    	tx[2] = 1;
    	usbuart_write(tx, 33);
    } else if (buf[0] == S_CMD_Q_BUSTYPE){
    	// serprog: Bus support: parallel=on, LPC=on, FWH=on, SPI=off
    	//tx[0] = BUS_FWH;
		tx[0] = S_ACK;
    	tx[1] = BUS_FWH;
    	usbuart_write(tx, 1);
    }
}