#include <string.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/scb.h>

#include "usbdfu.h"
#include "general.h"
#include "platform.h"
#include "ssd1306.h"

uint32_t app_address = 0x08000000;

extern uint32_t _stack;

int main(void)
{
    stm_hardware_init();
    //ssd1306_init();
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);    

    dfu_protect(UPD_MODE);
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
    systick_set_reload(450000);

    systick_interrupt_enable();
    systick_counter_enable();

    dfu_init(&st_usbfs_v1_usb_driver, UPD_MODE);

    dfu_main();
}

void dfu_detach(void)
{
    platform_request_boot();
    scb_reset_core();
}

void dfu_event(void)
{
}

void sys_tick_handler(void)
{
    gpio_toggle(GPIOC, GPIO13);
}
