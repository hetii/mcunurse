#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

void i2c_setup(void) {

  // AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_FULL_SWJ_NO_JNTRST;
  // Enable remapping of I2C1
  AFIO_MAPR |= AFIO_MAPR_I2C1_REMAP;
  
  // set clock for I2C1
  rcc_periph_clock_enable(RCC_I2C1);

  // Set alternate functions for the SCL and SDA pins of I2C1.
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_I2C1_RE_SCL | GPIO_I2C1_RE_SDA);

  // Disable the I2C before changing any configuration.
  i2c_peripheral_disable(I2C1);

  // APB1 is running at 36MHz.
  i2c_set_clock_frequency(I2C1, I2C_CR2_FREQ_36MHZ);

  // 400KHz - I2C Fast Mode
  i2c_set_fast_mode(I2C1);

  // fclock for I2C is 36MHz APB2 -> cycle time 28ns, low time at 400kHz
  // incl trise -> Thigh = 1600ns; CCR = tlow/tcycle = 0x1C,9;
  // Datasheet suggests 0x1e.
  i2c_set_ccr(I2C1, 0x1e);

  // fclock for I2C is 36MHz -> cycle time 28ns, rise time for
  // 400kHz => 300ns and 100kHz => 1000ns; 300ns/28ns = 10;
  // Incremented by 1 -> 11.
  i2c_set_trise(I2C1, 0x0b);

  // Enable ACK on I2C
  i2c_enable_ack(I2C1);

  // This is our slave address - needed only if we want to receive from
  // other masters.
  i2c_set_own_7bit_slave_address(I2C1, 0x32);

  // If everything is configured -> enable the peripheral.
  i2c_peripheral_enable(I2C1);
}

void stm_hardware_init(void)
{
    // Switch back to HSI.
    while (RCC_CFGR & 0xf){
        RCC_CFGR &= ~3;
    }

    //rcc_periph_clock_enable(RCC_GPIOA|RCC_GPIOB|RCC_GPIOC|RCC_USB|RCC_AFIO|RCC_CRC);
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);

    rcc_periph_clock_enable(RCC_USB);
    rcc_periph_reset_pulse(RST_USB);
    rcc_periph_clock_enable(RCC_AFIO);
    rcc_periph_clock_enable(RCC_CRC);

    gpio_clear(GPIOA, GPIO12);
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_OPENDRAIN, GPIO12);
    
    i2c_setup();
}
