CROSS_COMPILE ?= arm-none-eabi-
CC = $(CROSS_COMPILE)gcc
OBJCOPY = $(CROSS_COMPILE)objcopy

OPT_FLAGS = -Os

LDFLAGS += -L platforms/mcunurse/oled

CFLAGS += -mcpu=cortex-m3 -mthumb \
	-DSTM32F1 -DDISCOVERY_STLINK -I../libopencm3/include \
	-I platforms/stm32 -I platforms/mcunurse/oled -I platforms/mcunurse/stk500v2 -I platforms/mcunurse/serprog

LDFLAGS_BOOT := $(LDFLAGS) --specs=nano.specs \
	-lopencm3_stm32f1 -Wl,--defsym,_stack=0x20005000 \
	-Wl,-T,platforms/stm32/stlink.ld -nostartfiles -lc \
	-Wl,-Map=mapfile -mthumb -mcpu=cortex-m3 -Wl,-gc-sections \
	-L../libopencm3/lib 

LDFLAGS = $(LDFLAGS_BOOT) -Wl,-Ttext=0x8002000

ifeq ($(ENABLE_DEBUG), 1)
LDFLAGS += --specs=rdimon.specs
else
LDFLAGS += --specs=nosys.specs
endif

VPATH += platforms/stm32 platforms/mcunurse/oled platforms/mcunurse/stk500v2 platforms/mcunurse/flashrom

SRC = main.c platform.c
SRC += 	cdcacm.c	\
	usbuart.c 	\
	serialno.c	\
	timing.c	\
	timing_stm32.c	\
	traceswoasync.c	\
	hardware.c \
	common.c \
	ssd1306.c \
	ssd1306_fonts.c \
	stk500v2_api.c \
	stk500v2_cmd.c \
	stk500v2_fifo.c \
	stk500v2_hvpp.c \
	stk500v2_hvsp.c \
	stk500v2_isp.c \
	stk500v2_tpi.c \
	serprog_fifo.c \

all: blackmagic.bin blackmagic_dfu.bin blackmagic_dfu.hex dfu_upgrade.bin dfu_upgrade.hex

blackmagic_dfu: usbdfu.o dfucore.o dfu_f1.o hardware.o common.o
	@echo "  LD      $@"
	$(Q)$(CC) $^ -o $@ $(LDFLAGS_BOOT)

dfu_upgrade: dfu_upgrade.o dfucore.o dfu_f1.o hardware.o common.o
	@echo "  LD      $@"
	$(Q)$(CC) $^ -o $@ $(LDFLAGS)

host_clean:
	-$(Q)$(RM) blackmagic.bin blackmagic_dfu blackmagic_dfu.bin blackmagic_dfu.hex dfu_upgrade dfu_upgrade.bin dfu_upgrade.hex

flash_dfu_via_uart:
	@sudo python2.7 ../scripts/stm32loader.py -p /dev/ttyUSB0 -e -w -v blackmagic_dfu.bin

flash_big:
	@sudo python2.7 ../scripts/stm32_mem.py blackmagic.bin

flash_dfu_upgrade:
	@sudo dfu-util -s 0x08005000:leave -D dfu_upgrade.bin
	#@sudo dfu-util -s 0x08002000:leave -D dfu_upgrade.bin

flash_blackmagic_dfu:
	@sudo dfu-util -s 0x08000000 -D blackmagic_dfu.bin
		
flash_stm:
	@sudo dfu-util -d 1d50:6018,:6017 -s 0x08002000:leave -D blackmagic.bin

flash_stm2:
	@sudo dfu-util -d dead:ca5d -s 0x08001000:leave -D blackmagic.bin


install_stuff:
	@sudo pip2 install pyserial
	@sudo apt install gcc-arm-none-eabi
	@sudo apt install dfu-util

