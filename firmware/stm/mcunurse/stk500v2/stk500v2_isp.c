#include <stdio.h>
#include <stdint.h>
#include "stk500v2_isp.h"
#include "stk500v2_proto.h"

static uint8_t cmd_buffer[4];

extern void delay_ms(uint32_t delay);
extern void spi_init(void);
extern void spi_deinit(void);
extern void spi_select(void);
extern void spi_deselect(void);
extern uint8_t spi_transfer(uint8_t *data, uint8_t len);

uint8_t stk500v2_isp_enter_prog_mode(stk_enter_prog_isp_t *rx, stk_answer_t *tx){
    uint8_t rval;
    printf("stk500v2_isp_enter_prog_mode\n");
    // Configure and init SPI here!
    // Enable buffer if any!
    // stk500Delay == stkParam.bytes[24]
    // spi_set_speed(stk500Delay < 15 ? know_dividers[stk500Delay] : know_dividers[0]);
    spi_init();

    // Positive reset pulse:
    spi_select();   // RST low
    delay_ms(rx->stab_delay ? rx->stab_delay : 5);
    spi_deselect(); // RST high
    delay_ms(rx->stab_delay ? rx->stab_delay : 5);
    spi_select();   // RST low

    // When this command takes longer then rx->timeout
    // we should set tx->status = STK_STATUS_CMD_TOUT
    // but I keep it simple to reduce code.
    // Also rx->byte_delay is not implemented.
    delay_ms(rx->cmd_exe_delay);
    for (uint8_t i = rx->synch_loops ? rx->synch_loops : 32; i--;){
        rval = spi_transfer(rx->cmd, rx->poll_index);
        if (rx->poll_index < 4){
            spi_transfer(rx->cmd + rx->poll_index, 4 - rx->poll_index);
        }
        if (rval == rx->poll_value){
            printf("Success: We are in sync with AVR !\n");
            tx->status = STK_STATUS_CMD_OK;
            return 1;
        }
    }
    // If we are here it means that we are out of sync :(
    // Avrdude will call next stk500v2_isp_leave_prog_mode, 
    // so we can just exit here.
    tx->status = STK_STATUS_CMD_FAILED;
    return 1;
}

uint8_t stk500v2_isp_leave_prog_mode(stk_leave_prog_isp_t *rx, stk_answer_t *tx){
    printf("stk500v2_isp_leave_prog_mode\n");
    spi_deselect(); // RST high
    delay_ms(rx->pre_delay);
    spi_deinit();   // Disable buffer if any.
    delay_ms(rx->post_delay);
    tx->status = STK_STATUS_CMD_OK;
    return 1;
}

static unsigned char deviceIsBusy(void){
    cmd_buffer[0] = 0xf0;
    cmd_buffer[1] = 0;
    return spi_transfer(cmd_buffer, 4) & 1;
}

static unsigned char waitUntilReady(unsigned char msTimeout){
    //timerSetupTimeout(msTimeout);
    while(deviceIsBusy()){
      //  if(timerTimeoutOccurred())
        //    return STK_STATUS_RDY_BSY_TOUT;
    }
    return STK_STATUS_CMD_OK;
}

uint8_t stk500v2_isp_chip_erase(stk_chip_erase_isp_t *param, stk_answer_t *tx){
    tx->status = STK_STATUS_CMD_OK;
    spi_transfer(param->cmd, 4);
    if(param->poll_method != 0){
        // allow at least 10 ms 
        if(param->erase_delay < 10){
            tx->status = waitUntilReady(10);
        } else {
            tx->status = waitUntilReady(param->erase_delay);
        }
        delay_ms(100);
    } else { 
        delay_ms(param->erase_delay);
    }
    return 1;
}

uint8_t stk500v2_isp_program_memory(stk_program_flash_isp_t *param, stk_answer_t *tx, uint8_t is_eeprom){
    utilWord_t num_bytes;
    uint8_t rval = STK_STATUS_CMD_OK;
    uint8_t valuePollingMask, rdyPollingMask;
    unsigned int i;

    num_bytes.bytes[1] = param->num_bytes[0];
    num_bytes.bytes[0] = param->num_bytes[1];
    // page mode
    if (param->mode & 1){  
        valuePollingMask = 0x20;
        rdyPollingMask = 0x40;
    // word mode
    } else {
        valuePollingMask = 4;
        rdyPollingMask = 8;
    }

    if (!is_eeprom && stk_address.bytes[3] & 0x80){
        cmd_buffer[0] = 0x4d;
        cmd_buffer[1] = 0x00;
        cmd_buffer[2] = stk_address.bytes[2];
        cmd_buffer[3] = 0x00;
        spi_transfer(cmd_buffer, 4);
    }

    for (i = 0; rval == STK_STATUS_CMD_OK && i < num_bytes.word; i++){
        uint8_t x;
        cmd_buffer[1] = stk_address.bytes[1];
        cmd_buffer[2] = stk_address.bytes[0];
        cmd_buffer[3] = param->data[i];

        x = param->cmd[0];
        if (!is_eeprom){
            x &= ~0x08;
            if ((uint8_t)i & 1){
                x |= 0x08;
                stk_address.dword++;
            }
        } else {
            stk_address.dword++;
        }
        cmd_buffer[0] = x;
        spi_transfer(cmd_buffer, 4);
        // is page mode
        if(param->mode & 1){
            // not last byte written
            if(i < num_bytes.word - 1 || !(param->mode & 0x80))
                continue; 
            // write program memory page 
            cmd_buffer[0] = param->cmd[1];
            spi_transfer(cmd_buffer, 4);
        }
        
	// poll for ready after each byte (word mode) or page (page mode)
        // value polling
        if (param->mode & valuePollingMask){ 
            uint8_t d = param->data[i];
            // must use timed polling
            if (d == param->poll[0] || (is_eeprom && d == param->poll[1])){
               delay_ms(param->delay);
            } else {
                // read memory
                uint8_t x = param->cmd[2];
                if(!is_eeprom){
                    x &= ~0x08;
                    if((uint8_t)i & 1){
                        x |= 0x08;
                    }
                }
                cmd_buffer[0] = x;
                //timerSetupTimeout(param->delay);

                while(spi_transfer(cmd_buffer, 4) != d){
                /*
                    if(timerTimeoutOccurred()){
                        rval = STK_STATUS_CMD_TOUT;
                        break;
                    }
                */
                }
            }
        // rdy/bsy polling
        } else if (param->mode & rdyPollingMask){
            rval = waitUntilReady(param->delay);
            printf("rval: 0x%02x\n", rval);
        // must be timed delay 
        } else {
            delay_ms(param->delay);
        }
    }
    tx->status = rval;
    return 1;
}

// uint16_t bytes = ((uint16_t)rx->num_bytes[0] << 8) | (rx->num_bytes[1] & 0xff);
/*int16_t i = 0x1234;
uint16_t n = i; // because shifting the sign bit invokes UB
int8_t hi = ((i >> 8) & 0xff);
int8_t lo = ((i >> 0) & 0xff);*/
uint16_t stk500v2_isp_read_memory(stk_read_flash_isp_t *rx, stk_read_flash_isp_result_t *tx, uint8_t is_eeprom){
    uint8_t *p = tx->data;
    uint16_t bytes = ((uint16_t)rx->num_bytes[0] << 8) | (rx->num_bytes[1] & 0xff);
    /*utilWord_t bytes;
    bytes.bytes[1] = rx->num_bytes[0];
    bytes.bytes[0] = rx->num_bytes[1];*/
    // uword_t bytes;
    // bytes.hi = rx->num_bytes[0];
    // bytes.lo = rx->num_bytes[1];

    cmd_buffer[3] = 0;

    if(!is_eeprom && stk_address.bytes[3] & 0x80){
        cmd_buffer[0] = 0x4d; // load extended address
        cmd_buffer[1] = 0x00;
        cmd_buffer[2] = stk_address.bytes[2];
        spi_transfer(cmd_buffer, 4);
    }

    cmd_buffer[0] = rx->cmd;
    for(uint16_t i = 0; i < bytes; i++){
        cmd_buffer[1] = stk_address.bytes[1];
        cmd_buffer[2] = stk_address.bytes[0];
        if(!is_eeprom){
            if((uint16_t)i & 1){
                cmd_buffer[0] |= 0x08;
                stk_address.dword++;
            } else {
                cmd_buffer[0] &= ~0x08;
            }
        } else {
            stk_address.dword++;
        }
        *p++ = spi_transfer(cmd_buffer, 4);
    }
    // set status1 and status2
    tx->status1 = *p = STK_STATUS_CMD_OK; 
    return bytes + 2;
}

uint8_t stk500v2_isp_program_fuse(stk_program_fuse_isp_t *param, stk_answer_t *answer){
    spi_transfer(param->cmd, 4);
    answer->status = STK_STATUS_CMD_OK;
    return 1;
}

uint8_t stk500v2_isp_read_fuse(stk_read_fuse_isp_t *param, stk_answer_t *answer){
    answer->value = spi_transfer(param->cmd, param->ret_addr);
    if(param->ret_addr < 4){
        spi_transfer(param->cmd + param->ret_addr, 4 - param->ret_addr);
    }
    answer->status = STK_STATUS_CMD_OK;
    return 2;
}

uint8_t stk500v2_isp_spi_multi(stk_multi_isp_t *param, stk_multi_isp_result_t *result){
    uint8_t cnt1, i, *p;
    printf("MULTI CALLED\n");
    cnt1 = param->num_tx;
    if(cnt1 > param->rx_start_addr){
        cnt1 = param->rx_start_addr;
    }

    spi_transfer(param->tx_data, cnt1);

    p = result->rx_data;
    for (i = 0; i < param->num_tx - cnt1; i++){
        uint8_t b = spi_transfer(&param->tx_data[cnt1] + i, 1);
        if(i < param->num_rx){
            *p++ = b;
        }
    }
    
    for (; i < param->num_rx; i++){
        cmd_buffer[0] = 0;
        *p++ = spi_transfer(cmd_buffer, 1);
    }
    *p = result->status1 = STK_STATUS_CMD_OK;
    //answer->status = STK_STATUS_CMD_OK;
    return (uint8_t)param->num_rx + 2;
}
// https://github.com/adafruit/ftdiflash/blob/master/ftdiflash.c
