// *****************[ STK message constants ]***************************
#ifndef _STK500V2_PROTO_
#define _STK500V2_PROTO_

#define MESSAGE_START                       0x1B        //= ESC = 27 decimal
#define TOKEN                               0x0E

// *****************[ STK general command constants ]**************************

#define STK_CMD_SIGN_ON                         0x01
#define STK_CMD_SET_PARAMETER                   0x02
#define STK_CMD_GET_PARAMETER                   0x03
#define STK_CMD_SET_DEVICE_PARAMETERS           0x04
#define STK_CMD_OSCCAL                          0x05
#define STK_CMD_LOAD_ADDRESS                    0x06
#define STK_CMD_FIRMWARE_UPGRADE                0x07
#define STK_CMD_CHECK_TARGET_CONNECTION         0x0D
#define STK_CMD_LOAD_RC_ID_TABLE                0x0E
#define STK_CMD_LOAD_EC_ID_TABLE                0x0F


// *****************[ STK ISP command constants ]******************************

#define STK_CMD_ENTER_PROGMODE_ISP              0x10
#define STK_CMD_LEAVE_PROGMODE_ISP              0x11
#define STK_CMD_CHIP_ERASE_ISP                  0x12
#define STK_CMD_PROGRAM_FLASH_ISP               0x13
#define STK_CMD_READ_FLASH_ISP                  0x14
#define STK_CMD_PROGRAM_EEPROM_ISP              0x15
#define STK_CMD_READ_EEPROM_ISP                 0x16
#define STK_CMD_PROGRAM_FUSE_ISP                0x17
#define STK_CMD_READ_FUSE_ISP                   0x18
#define STK_CMD_PROGRAM_LOCK_ISP                0x19
#define STK_CMD_READ_LOCK_ISP                   0x1A
#define STK_CMD_READ_SIGNATURE_ISP              0x1B
#define STK_CMD_READ_OSCCAL_ISP                 0x1C
#define STK_CMD_SPI_MULTI                       0x1D

// *****************[ STK PP command constants ]*******************************

#define STK_CMD_ENTER_PROGMODE_PP               0x20
#define STK_CMD_LEAVE_PROGMODE_PP               0x21
#define STK_CMD_CHIP_ERASE_PP                   0x22
#define STK_CMD_PROGRAM_FLASH_PP                0x23
#define STK_CMD_READ_FLASH_PP                   0x24
#define STK_CMD_PROGRAM_EEPROM_PP               0x25
#define STK_CMD_READ_EEPROM_PP                  0x26
#define STK_CMD_PROGRAM_FUSE_PP                 0x27
#define STK_CMD_READ_FUSE_PP                    0x28
#define STK_CMD_PROGRAM_LOCK_PP                 0x29
#define STK_CMD_READ_LOCK_PP                    0x2A
#define STK_CMD_READ_SIGNATURE_PP               0x2B
#define STK_CMD_READ_OSCCAL_PP                  0x2C

#define STK_CMD_SET_CONTROL_STACK               0x2D

// *****************[ STK HVSP command constants ]*****************************

#define STK_CMD_ENTER_PROGMODE_HVSP             0x30
#define STK_CMD_LEAVE_PROGMODE_HVSP             0x31
#define STK_CMD_CHIP_ERASE_HVSP                 0x32
#define STK_CMD_PROGRAM_FLASH_HVSP              0x33
#define STK_CMD_READ_FLASH_HVSP                 0x34
#define STK_CMD_PROGRAM_EEPROM_HVSP             0x35
#define STK_CMD_READ_EEPROM_HVSP                0x36
#define STK_CMD_PROGRAM_FUSE_HVSP               0x37
#define STK_CMD_READ_FUSE_HVSP                  0x38
#define STK_CMD_PROGRAM_LOCK_HVSP               0x39
#define STK_CMD_READ_LOCK_HVSP                  0x3A
#define STK_CMD_READ_SIGNATURE_HVSP             0x3B
#define STK_CMD_READ_OSCCAL_HVSP                0x3C
// These two are redefined since 0x30/0x31 collide
// with the STK600 bootloader.
#define STK_CMD_ENTER_PROGMODE_HVSP_STK600      0x3D
#define STK_CMD_LEAVE_PROGMODE_HVSP_STK600      0x3E

// *** XPROG command constants ***

#define STK_CMD_XPROG                           0x50
#define STK_CMD_XPROG_SETMODE                   0x51


// *** AVR32 JTAG Programming command ***

#define STK_CMD_JTAG_AVR32                      0x80
#define STK_CMD_ENTER_PROGMODE_JTAG_AVR32       0x81
#define STK_CMD_LEAVE_PROGMODE_JTAG_AVR32       0x82


// *** AVR JTAG Programming command ***

#define STK_CMD_JTAG_AVR                        0x90

// *****************[ STK test command constants ]***************************

#define STK_CMD_ENTER_TESTMODE                  0x60
#define STK_CMD_LEAVE_TESTMODE                  0x61
#define STK_CMD_CHIP_WRITE                      0x62
#define STK_CMD_PROGRAM_FLASH_PARTIAL           0x63
#define STK_CMD_PROGRAM_EEPROM_PARTIAL          0x64
#define STK_CMD_PROGRAM_SIGNATURE_ROW           0x65
#define STK_CMD_READ_FLASH_MARGIN               0x66
#define STK_CMD_READ_EEPROM_MARGIN              0x67
#define STK_CMD_READ_SIGNATURE_ROW_MARGIN       0x68
#define STK_CMD_PROGRAM_TEST_FUSE               0x69
#define STK_CMD_READ_TEST_FUSE                  0x6A
#define STK_CMD_PROGRAM_HIDDEN_FUSE_LOW         0x6B
#define STK_CMD_READ_HIDDEN_FUSE_LOW            0x6C
#define STK_CMD_PROGRAM_HIDDEN_FUSE_HIGH        0x6D
#define STK_CMD_READ_HIDDEN_FUSE_HIGH           0x6E
#define STK_CMD_PROGRAM_HIDDEN_FUSE_EXT         0x6F
#define STK_CMD_READ_HIDDEN_FUSE_EXT            0x70

// *****************[ STK status constants ]***************************

// Success
#define STK_STATUS_CMD_OK                       0x00

// Warnings
#define STK_STATUS_CMD_TOUT                     0x80
#define STK_STATUS_RDY_BSY_TOUT                 0x81
#define STK_STATUS_SET_PARAM_MISSING            0x82

// Errors
#define STK_STATUS_CMD_FAILED                   0xC0
#define STK_STATUS_CKSUM_ERROR                  0xC1
#define STK_STATUS_CMD_UNKNOWN                  0xC9

// *****************[ STK parameter constants ]***************************

/* ScratchMonkey parameter */
#define STK_PARAM_SCRATCHMONKEY_STATUS_LEDS     0x2A
enum {
    SCRATCHMONKEY_RDY_LED   = (1<<0),
    SCRATCHMONKEY_PGM_LED   = (1<<1),
    SCRATCHMONKEY_VFY_LED   = (1<<2),
    SCRATCHMONKEY_ERR_LED   = (1<<3)
};

/* STK500v2 parameters */
#define STK_PARAM_BUILD_NUMBER_LOW              0x80
#define STK_PARAM_BUILD_NUMBER_HIGH             0x81
#define STK_PARAM_HW_VER                        0x90
#define STK_PARAM_SW_MAJOR                      0x91
#define STK_PARAM_SW_MINOR                      0x92
#define STK_PARAM_VTARGET                       0x94
#define STK_PARAM_VADJUST                       0x95
#define STK_PARAM_OSC_PSCALE                    0x96
#define STK_PARAM_OSC_CMATCH                    0x97
#define STK_PARAM_SCK_DURATION                  0x98
#define STK_PARAM_TOPCARD_DETECT                0x9A
#define STK_PARAM_STATUS                        0x9C
#define STK_PARAM_DATA                          0x9D
#define STK_PARAM_RESET_POLARITY                0x9E
#define STK_PARAM_CONTROLLER_INIT               0x9F

/* STK600 parameters */
#define STK_PARAM_STATUS_TGT_CONN               0xA1
#define STK_PARAM_DISCHARGEDELAY                0xA4
#define STK_PARAM_SOCKETCARD_ID                 0xA5
#define STK_PARAM_ROUTINGCARD_ID                0xA6
#define STK_PARAM_EXPCARD_ID                    0xA7
#define STK_PARAM_SW_MAJOR_SLAVE1               0xA8
#define STK_PARAM_SW_MINOR_SLAVE1               0xA9
#define STK_PARAM_SW_MAJOR_SLAVE2               0xAA
#define STK_PARAM_SW_MINOR_SLAVE2               0xAB
#define STK_PARAM_BOARD_ID_STATUS               0xAD
#define STK_PARAM_RESET                         0xB4

#define STK_PARAM_JTAG_ALLOW_FULL_PAGE_STREAM   0x50
#define STK_PARAM_JTAG_EEPROM_PAGE_SIZE         0x52
#define STK_PARAM_JTAG_DAISY_BITS_BEFORE        0x53
#define STK_PARAM_JTAG_DAISY_BITS_AFTER         0x54
#define STK_PARAM_JTAG_DAISY_UNITS_BEFORE       0x55
#define STK_PARAM_JTAG_DAISY_UNITS_AFTER        0x56

// *** Parameter constants for 2 byte values ***
#define STK_PARAM2_SCK_DURATION                 0xC0
#define STK_PARAM2_CLOCK_CONF                   0xC1
#define STK_PARAM2_AREF0                        0xC2
#define STK_PARAM2_AREF1                        0xC3

#define STK_PARAM2_JTAG_FLASH_SIZE_H            0xC5
#define STK_PARAM2_JTAG_FLASH_SIZE_L            0xC6
#define STK_PARAM2_JTAG_FLASH_PAGE_SIZE         0xC7
#define STK_PARAM2_RC_ID_TABLE_REV              0xC8
#define STK_PARAM2_EC_ID_TABLE_REV              0xC9

/* STK600 XPROG section */
// XPROG modes
#define XPRG_MODE_PDI                       0
#define XPRG_MODE_JTAG                      1
#define XPRG_MODE_TPI                       2

// XPROG commands
#define XPRG_CMD_ENTER_PROGMODE             0x01
#define XPRG_CMD_LEAVE_PROGMODE             0x02
#define XPRG_CMD_ERASE                      0x03
#define XPRG_CMD_WRITE_MEM                  0x04
#define XPRG_CMD_READ_MEM                   0x05
#define XPRG_CMD_CRC                        0x06
#define XPRG_CMD_SET_PARAM                  0x07

// Memory types
#define XPRG_MEM_TYPE_APPL                   1
#define XPRG_MEM_TYPE_BOOT                   2
#define XPRG_MEM_TYPE_EEPROM                 3
#define XPRG_MEM_TYPE_FUSE                   4
#define XPRG_MEM_TYPE_LOCKBITS               5
#define XPRG_MEM_TYPE_USERSIG                6
#define XPRG_MEM_TYPE_FACTORY_CALIBRATION    7

// Erase types
#define XPRG_ERASE_CHIP                      1
#define XPRG_ERASE_APP                       2
#define XPRG_ERASE_BOOT                      3
#define XPRG_ERASE_EEPROM                    4
#define XPRG_ERASE_APP_PAGE                  5
#define XPRG_ERASE_BOOT_PAGE                 6
#define XPRG_ERASE_EEPROM_PAGE               7
#define XPRG_ERASE_USERSIG                   8
#define XPRG_ERASE_CONFIG                    9  // TPI only, prepare fuse write

// Write mode flags
#define XPRG_MEM_WRITE_ERASE                 0
#define XPRG_MEM_WRITE_WRITE                 1

// CRC types
#define XPRG_CRC_APP                         1
#define XPRG_CRC_BOOT                        2
#define XPRG_CRC_FLASH                       3

// Error codes
#define XPRG_ERR_OK                          0
#define XPRG_ERR_FAILED                      1
#define XPRG_ERR_COLLISION                   2
#define XPRG_ERR_TIMEOUT                     3

// XPROG parameters of different sizes
// 4-byte address
#define XPRG_PARAM_NVMBASE                  0x01
// 2-byte page size
#define XPRG_PARAM_EEPPAGESIZE              0x02
// 1-byte, undocumented TPI param
#define XPRG_PARAM_TPI_3                    0x03
// 1-byte, undocumented TPI param
#define XPRG_PARAM_TPI_4                    0x04

// *****************[ STK answer constants ]***************************

#define STK_ANSWER_CKSUM_ERROR              0xB0

#endif