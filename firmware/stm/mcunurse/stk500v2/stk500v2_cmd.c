#include <stdint.h>
#include <stdio.h>
#include "stk500v2_cmd.h"
#include "stk500v2_proto.h"

stk_param_t stk_param = {{
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 2, 4, 0, 50, 0, 1, 0x80,
//   \  \  \_STK VERSION MINOR
//    \  \___STK VERSION MAJOR
//     \_____STK VERSION HW
    11, 0, 0xaa, 0, 0, 0, 0, 0, 
//    \______stk500Delay for ispAttachToDevice when avrdude is not called with -B argument.
}};

utilDword_t stk_address;

uint8_t stk500v2_sign_on(stk_sign_on_t *answer){
    answer->status = STK_STATUS_CMD_OK;
    answer->lenght = 8;
    answer->body[0] = 'S';
    answer->body[1] = 'T';
    answer->body[2] = 'K';
    answer->body[3] = '5';
    answer->body[4] = '0';
    answer->body[5] = '0';
    answer->body[6] = '_';
    answer->body[7] = '2';
    return 10;
}

uint8_t stk500v2_set_param(uint8_t index, uint8_t value, stk_answer_t *answer){
    stk_param.bytes[index & 0x1f] = value;
    answer->status = STK_STATUS_CMD_OK;
    /*if (index > 0x1f){
        answer->status = STK_STATUS_CMD_FAILED;
    } else {
        stk_param.bytes[index & 0x1f] = value;
        answer->status = STK_STATUS_CMD_OK;
    }*/
    return 1;
}

uint8_t stk500v2_get_param(uint8_t index, stk_answer_t *answer){
    answer->status = STK_STATUS_CMD_OK;
    answer->value  = stk_param.bytes[index & 0x1f];
    return 2;
    /*if (index > 0x1f){
        printf("DUPA stk500v2_get_param: %d\n", index);
        answer->status = STK_STATUS_CMD_FAILED;
        return 1;
    } else {
        answer->status = STK_STATUS_CMD_OK;
        answer->value  = stk_param.bytes[index & 0x1f];
        return 2;
    }*/
}

uint8_t stk500v2_load_address(stk_address_t *rx, stk_answer_t *tx){
    /*printf("STK_CMD_LOAD_ADDRESS 0: 0x%02x\n", rx->address[0]);
    printf("STK_CMD_LOAD_ADDRESS 1: 0x%02x\n", rx->address[1]);
    printf("STK_CMD_LOAD_ADDRESS 2: 0x%02x\n", rx->address[2]);
    printf("STK_CMD_LOAD_ADDRESS 3: 0x%02x\n", rx->address[3]);
    printf("\n");*/
    stk_address.bytes[3] = rx->address[0];
    stk_address.bytes[2] = rx->address[1];
    stk_address.bytes[1] = rx->address[2];
    stk_address.bytes[0] = rx->address[3];
    tx->status = STK_STATUS_CMD_OK;
    return 1;
}

void stk500v2_set_control_stack(void)
{
    printf("STK_CMD_SET_CONTROL_STACK!\n");
}
