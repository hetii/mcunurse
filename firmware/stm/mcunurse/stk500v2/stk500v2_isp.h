#ifndef _STK500V2_ISP_
#define _STK500V2_ISP_

#include "stk500v2_cmd.h"

typedef struct stk_enter_prog_isp{
    uint8_t timeout;
    uint8_t stab_delay;
    uint8_t cmd_exe_delay;
    uint8_t synch_loops;
    uint8_t byte_delay;
    uint8_t poll_value;
    uint8_t poll_index;
    uint8_t cmd[4];
} stk_enter_prog_isp_t;

typedef struct stk_leave_prog_isp{
    uint8_t pre_delay;
    uint8_t post_delay;
} stk_leave_prog_isp_t;

typedef struct stk_chip_erase_isp{
    uint8_t erase_delay;
    uint8_t poll_method;
    uint8_t cmd[4];
} stk_chip_erase_isp_t;

typedef struct stk_program_flash_isp{
    uint8_t num_bytes[2];
    uint8_t mode;
    uint8_t delay;
    uint8_t cmd[3];
    uint8_t poll[2];
    uint8_t data[1]; // actually more data than 1 byte
} stk_program_flash_isp_t;

typedef struct stk_read_flash_isp{
    uint8_t num_bytes[2];
    uint8_t cmd;
} stk_read_flash_isp_t;

typedef struct stk_read_flash_isp_result{
    uint8_t status1;
    uint8_t data[1]; // actually more than 1 byte
    //uint8_t status2
} stk_read_flash_isp_result_t;

typedef struct stk_program_fuse_isp{
    uint8_t cmd[4];
} stk_program_fuse_isp_t;

typedef struct stk_read_fuse_isp{
    uint8_t ret_addr;
    uint8_t cmd[4];
} stk_read_fuse_isp_t;
 
typedef struct stk_multi_isp{
    uint8_t num_tx;
    uint8_t num_rx;
    uint8_t rx_start_addr;
    uint8_t tx_data[1]; // actually more than 1 byte
} stk_multi_isp_t;

typedef struct stk_multi_isp_result{
    uint8_t status1;
    uint8_t rx_data[1]; // potentially more than 1 byte
    //uint8_t status2
} stk_multi_isp_result_t;

uint8_t stk500v2_isp_enter_prog_mode(stk_enter_prog_isp_t *param, stk_answer_t *answer);
uint8_t stk500v2_isp_leave_prog_mode(stk_leave_prog_isp_t *param, stk_answer_t *answer);
uint8_t stk500v2_isp_chip_erase(stk_chip_erase_isp_t *param, stk_answer_t *answer);
uint8_t stk500v2_isp_spi_multi(stk_multi_isp_t *param, stk_multi_isp_result_t *result);
//uint8_t stk500v2_isp_read_memory(stk_read_flash_isp_t *rx, stk_read_flash_isp_result_t *tx, uint8_t is_eeprom);
uint16_t stk500v2_isp_read_memory(stk_read_flash_isp_t *rx, stk_read_flash_isp_result_t *tx, uint8_t is_eeprom);
uint8_t stk500v2_isp_program_memory(stk_program_flash_isp_t *rx, stk_answer_t *tx, uint8_t is_eeprom);
uint8_t stk500v2_isp_read_fuse(stk_read_fuse_isp_t *param, stk_answer_t *answer);
uint8_t stk500v2_isp_program_fuse(stk_program_fuse_isp_t *param, stk_answer_t *answer);
#endif