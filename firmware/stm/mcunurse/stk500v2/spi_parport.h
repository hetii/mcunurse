#ifndef _SPI_PARPORT_
#define _SPI_PARPORT_

#include <stdint.h>

uint8_t spi_transfer(uint8_t *data, uint8_t len);
void spi_init(void);
void spi_deinit(void);
void spi_select(void);
void spi_deselect(void);

#endif
