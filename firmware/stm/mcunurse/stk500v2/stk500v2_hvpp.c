#include "stk500v2_hvpp.h"

void stk500v2_hvpp_enter_prog_mode(void)
{

}

void stk500v2_hvpp_leave_prog_mode(void)
{

}

void stk500v2_hvpp_chip_erase(void)
{

}

void stk500v2_hvpp_program_flash(void)
{

}

void stk500v2_hvpp_read_flash(void)
{

}

void stk500v2_hvpp_program_eeprom(void)
{

}

void stk500v2_hvpp_read_eeprom(void)
{

}

void stk500v2_hvpp_program_fuse(void)
{

}

void stk500v2_hvpp_read_fuse(void)
{

}

void stk500v2_hvpp_program_lock(void)
{

}

void stk500v2_hvpp_read_lock(void)
{

}

void stk500v2_hvpp_read_signature(void)
{

}

void stk500v2_hvpp_read_osc_cal(void)
{

}