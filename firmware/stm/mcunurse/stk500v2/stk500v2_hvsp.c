#include "stk500v2_hvsp.h"

void stk500v2_hvsp_enter_prog_mode(void)
{

}
void stk500v2_hvsp_leave_prog_mode(void)
{

}
void stk500v2_hvsp_chip_erase(void)
{

}
void stk500v2_hvsp_program_flash(void)
{

}
void stk500v2_hvsp_read_flash(void)
{

}
void stk500v2_hvsp_program_eeprom(void)
{

}
void stk500v2_hvsp_read_eeprom(void)
{

}
void stk500v2_hvsp_program_fuse(void)
{

}
void stk500v2_hvsp_read_fuse(void)
{

}
void stk500v2_hvsp_program_lock(void)
{

}
void stk500v2_hvsp_read_lock(void)
{

}
void stk500v2_hvsp_read_signature(void)
{

}
void stk500v2_hvsp_read_osc_cal(void)
{

}
