#include "spi_parport.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/ppdev.h>
#include <sys/ioctl.h>

// STK200 lpt pinout:
#define MISO 6 // 10 - /ACK
#define MOSI 5 // 7  - Data 5
#define SCK  4 // 6  - Data 4
#define RST  7 // 9  - Data 7
#define G1   3 // 5  - Data 3
#define G2   2 // 4  - Data 2

#define DEVICE "/dev/parport0"
static int fd;

/*unsigned char spi_soft(unsigned char dat){
    unsigned char cnt = 8;
    while (cnt--) {
       if (dat & 0x80) PORTB |= (1<<MOSI);
       else PORTB &= ~(1<<MOSI);
       PORTB |= (1<<SCK);
       dat <<= 1;
       if (PINB & (1<<MISO)) dat++;
       PORTB &= ~(1<<SCK);
    }
    return dat;
}
*/

uint8_t spi_transfer_byte(uint8_t dat){
  uint8_t cnt = 8;
  uint8_t lpt_data = 0;
  uint8_t val = 0;
  while (cnt--){
    if (dat & 0x80){
      lpt_data |= (1<<MOSI);
    } else {
      lpt_data &= ~(1<<MOSI);
    }
    ioctl(fd, PPWDATA, &lpt_data);
    lpt_data |= (1<<SCK);
    ioctl(fd, PPWDATA, &lpt_data); 
    dat <<= 1;
    ioctl(fd, PPRSTATUS, &val);
    if (val & (1<<MISO)){
      //if (((val & (1<<MISO)) == 1<<MISO)){
      //if (((val & PARPORT_STATUS_ACK) == PARPORT_STATUS_ACK)){
      dat++;
    }
    lpt_data &= ~(1<<SCK);
    ioctl(fd, PPWDATA, &lpt_data);
  }
  return dat;
}

uint8_t spi_transfer(uint8_t *data, uint8_t len){
  uint8_t d=0;
  while(len--){
    d = spi_transfer_byte(*data++);
  }
  return d;
}

void spi_init(void){
  // Skip init if this method was called multiple time
  // before spi_deinit().
  if (fd > 0){
    return;
  }
  if ((fd=open(DEVICE, O_RDWR)) < 0){
    fprintf(stderr, "can not open %s\n", DEVICE);
  }
  if (ioctl(fd, PPCLAIM)){
    perror("PPCLAIM");
    close(fd);
  }
  // Enable buffor.
  uint8_t lpt_data = 0b11100011;
  ioctl(fd, PPWDATA, &lpt_data);
}

void spi_deinit(void){
  // Disable buffor.
  uint8_t lpt_data = 0b00001100;
  ioctl(fd, PPWDATA, &lpt_data);
  ioctl(fd, PPRELEASE);
  close(fd);
  fd = 0;
}

void spi_select(void){
  uint8_t lpt_data = 0xff;
  lpt_data &= ~((1<<G1)|(1<<G2)); // Enable buffor
  lpt_data &= ~(1<<SCK);          // SCK low
  lpt_data &= ~(1<<RST);          // RST low
  ioctl(fd, PPWDATA, &lpt_data);
}

void spi_deselect(void){
   uint8_t lpt_data = 0;
   lpt_data &= ~((1<<G1)|(1<<G2)); // Disable buffor
   lpt_data |= (1<<RST);           // RST high
   ioctl(fd, PPWDATA, &lpt_data);
}

