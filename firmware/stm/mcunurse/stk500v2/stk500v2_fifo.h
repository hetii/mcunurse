#ifndef _STK500V2_FIFO_
#define _STK500V2_FIFO_

#include <stdint.h>

typedef struct stk_msg{
    uint8_t start;
    uint8_t sequence;
    uint8_t size_msb;
    uint8_t size_lsb;
    uint8_t token;
    uint8_t cmd;
    uint8_t body[268]; // 257
    uint8_t checksum;
} stk_msg_t;

void stk500v2_poll(void);
void stk500v2_write_query(uint8_t *buf, uint16_t len);

extern void stk500v2_write_answer(uint8_t *buf, uint16_t len);
extern void stk500v2_write_spi(uint8_t *buf, uint16_t len);

#endif
