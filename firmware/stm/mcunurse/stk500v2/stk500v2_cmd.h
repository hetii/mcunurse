#ifndef _STK500V2_CMD_
#define _STK500V2_CMD_

typedef struct stk_sign_on{
    uint8_t status;
    uint8_t lenght;
    uint8_t body[8];
} stk_sign_on_t;

typedef struct stk_address{
    uint8_t address[4];
} stk_address_t;

typedef union{
    uint32_t dword; // uint32_t => 4 bytes
    uint8_t  bytes[4];
} utilDword_t;

typedef union{
    uint16_t word; // uint16_t => 2 bytes
    uint8_t  bytes[2];
} utilWord_t;

typedef union{
    uint16_t word; // uint16_t => 2 bytes
    uint8_t  lo;
    uint8_t  hi;
} uword_t;

typedef struct stk_answer{
    uint8_t status;
    uint8_t value;
} stk_answer_t;

typedef union{
    uint8_t bytes[32];
    struct{
        int build_version_low;
        uint8_t reserved1[14];
        uint8_t hardware_version;
        uint8_t software_version_major;
        uint8_t software_version_minor;
        uint8_t reserved2;
        uint8_t v_target;
        uint8_t v_ref;
        uint8_t osc_prescale;
        uint8_t osc_cmatch;
        uint8_t sck_duration;
        uint8_t reserved3;
        uint8_t topcard_detect;
        uint8_t reserved4;
        uint8_t status;
        uint8_t data;
        uint8_t reset_polarity;
        uint8_t controller_init;
    } s;
} stk_param_t;

uint8_t stk500v2_sign_on(stk_sign_on_t *answer);
uint8_t stk500v2_set_param(uint8_t index, uint8_t value, stk_answer_t *answer);
uint8_t stk500v2_get_param(uint8_t index, stk_answer_t *answer);
uint8_t stk500v2_load_address(stk_address_t *rx, stk_answer_t *tx);

void stk500v2_set_control_stack(void);

extern utilDword_t stk_address;

#endif
