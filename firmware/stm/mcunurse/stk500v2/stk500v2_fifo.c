#include <stdint.h>

#include <stdio.h> 

#include "stk500v2_fifo.h"
#include "stk500v2_cmd.h"
#include "stk500v2_hvpp.h"
#include "stk500v2_hvsp.h"
#include "stk500v2_isp.h"
#include "stk500v2_proto.h"
#include "stk500v2_tpi.h"

static uint8_t stk_msg[275];
static uint16_t stk_tx_index = 0;

// Implement this here to save space, quite minimalistic :D
static void *memcpy(void *dst, const void *src, size_t count) {
    uint8_t *dstb = (uint8_t*)dst;
    uint8_t *srcb = (uint8_t*)src;
    while (count--){
        *dstb++ = *srcb++;
    }
    return dst;
}

static void stk500v2_set_checksum(void){
    uint8_t sum = 0;
    uint8_t *p = stk_msg;
    stk_msg[0] = MESSAGE_START;
    stk_msg[2] = (stk_tx_index >> 8) & 0xff;
    stk_msg[3] = stk_tx_index & 0xff;
    stk_msg[4] = TOKEN;
    stk_tx_index += 6;
    for (uint16_t i = 1; i < stk_tx_index; ++i){
        sum ^= *p++;
    }
    *p = sum;
}

// This method assume to get whole buffer with payload.
void stk500v2_write_query(uint8_t *buf, uint16_t len){
    // Small checking if we have correct buffer size and control data - len == MSB+LSB+6
    uint16_t expected = ( (buf[2]<<8) | buf[3] ) + 6;
    // printf("Expected bytes: %d -> %d\n", expected, len);
    // if (stk_tx_index == 0 && buf[0] == MESSAGE_START && buf[4] == TOKEN && len == expected){
    if (buf[0] == MESSAGE_START && buf[4] == TOKEN && len == expected){
        // message is complete
        memcpy(stk_msg, buf, len);
        uint8_t sum = 0;
        uint8_t *p = stk_msg;
        while(len--){
            sum ^= *p++;
        }
        // Check if sum is correct. 
        if(sum != 0){
            printf("Checksum from avrdude message is invalid!\n");
            stk_msg[5] = stk_msg[6] = STK_ANSWER_CKSUM_ERROR;
            stk_tx_index = 2;
        }
    }
}

void stk500v2_poll(void){
    static uint8_t xprog_mode = XPRG_MODE_TPI;
    static uint8_t old_sequence = 0xFF;

    stk_msg_t *msg = (stk_msg_t *) &stk_msg;

    // As we always get first a query command, and then prepare answer on it,
    // we can use the same buffer for the data to reduce ram usage.
    //
    // Just take care and don`t overwrite some value before they are used.
    void *rx = &msg->body; // RX is a pointer to buffer with data for uC side.
    void *tx = &msg->body; // TX is a pointer to buffer with data for PC side. 

    if (old_sequence == msg->sequence){
        return;
    // If tx index is greate then 0 it means 
    // that we have data to push out.
    } else if (stk_tx_index > 0){
        old_sequence = msg->sequence;
        stk500v2_set_checksum();
        stk500v2_write_answer(stk_msg, stk_tx_index);
        stk_tx_index = 0;
        return; 
    }

    stk_tx_index = 1;
    //printf("MSG->cmd: 0x%02x\n", msg->cmd);
    switch (msg->cmd) {
        // General commands
        case STK_CMD_SIGN_ON:
            stk_tx_index += stk500v2_sign_on(rx);
            break;
        case STK_CMD_SET_PARAMETER:
            stk_tx_index += stk500v2_set_param(stk_msg[6], stk_msg[7], tx);
            break;
        case STK_CMD_GET_PARAMETER:
            stk_tx_index += stk500v2_get_param(stk_msg[6], tx);
            break;
        case STK_CMD_LOAD_ADDRESS:
            stk_tx_index += stk500v2_load_address(rx, tx);
            break;
        case STK_CMD_SET_CONTROL_STACK:
            stk500v2_set_control_stack();
            break;

        // ISP Commands
        case STK_CMD_ENTER_PROGMODE_ISP:
            stk_tx_index += stk500v2_isp_enter_prog_mode(rx, tx);
            break;
        case STK_CMD_LEAVE_PROGMODE_ISP:
            stk_tx_index += stk500v2_isp_leave_prog_mode(rx, tx);
            break;
        case STK_CMD_CHIP_ERASE_ISP:
            stk_tx_index += stk500v2_isp_chip_erase(rx, tx);
            break;
        case STK_CMD_PROGRAM_FLASH_ISP:
            stk_tx_index += stk500v2_isp_program_memory(rx, tx, 0);
            break;
        case STK_CMD_PROGRAM_EEPROM_ISP:
            stk_tx_index += stk500v2_isp_program_memory(rx, tx, 1);
            break;            
        case STK_CMD_READ_FLASH_ISP:
            stk_tx_index += stk500v2_isp_read_memory(rx, tx, 0);
            break;
        case STK_CMD_READ_EEPROM_ISP:
            stk_tx_index += stk500v2_isp_read_memory(rx, tx, 1);
            break;
        case STK_CMD_PROGRAM_FUSE_ISP:
        case STK_CMD_PROGRAM_LOCK_ISP:
            stk_tx_index += stk500v2_isp_program_fuse(rx, tx);
            break;       
        case STK_CMD_READ_FUSE_ISP:      // Following commands are basically the same as the read fuse command,
        case STK_CMD_READ_LOCK_ISP:      // only that ISP commands for reading the lock byte must be supplied.
        case STK_CMD_READ_SIGNATURE_ISP: // only that ISP commands for reading one of the signature bytes must be supplied.
        case STK_CMD_READ_OSCCAL_ISP:    // only that ISP commands for reading the OSCCAL byte must be supplied.
            stk_tx_index += stk500v2_isp_read_fuse(rx, tx);
            break;
        case STK_CMD_SPI_MULTI:
            stk_tx_index += stk500v2_isp_spi_multi(rx, tx);
            // len.word = 1 + ispMulti(param, (void *)&txBuffer[STK_TXMSG_START + 1]);
            break;

        // HVSP Commands
        case STK_CMD_ENTER_PROGMODE_HVSP:
        case STK_CMD_ENTER_PROGMODE_HVSP_STK600:
            stk500v2_hvsp_enter_prog_mode();
            break;
        case STK_CMD_LEAVE_PROGMODE_HVSP:
        case STK_CMD_LEAVE_PROGMODE_HVSP_STK600:
            stk500v2_hvsp_leave_prog_mode();
            break;
        case STK_CMD_CHIP_ERASE_HVSP:
            stk500v2_hvsp_chip_erase();
            break;
        case STK_CMD_PROGRAM_FLASH_HVSP:
            stk500v2_hvsp_program_flash();
            break;
        case STK_CMD_READ_FLASH_HVSP:
            stk500v2_hvsp_read_flash();
            break;
        case STK_CMD_PROGRAM_EEPROM_HVSP:
            stk500v2_hvsp_program_eeprom();
            break;
        case STK_CMD_READ_EEPROM_HVSP:
            stk500v2_hvsp_read_eeprom();
            break;
        case STK_CMD_PROGRAM_FUSE_HVSP:
            stk500v2_hvsp_program_fuse();
            break;
        case STK_CMD_READ_FUSE_HVSP:
            stk500v2_hvsp_read_fuse();
            break;
        case STK_CMD_PROGRAM_LOCK_HVSP:
            stk500v2_hvsp_program_lock();
            break;
        case STK_CMD_READ_LOCK_HVSP:
            stk500v2_hvsp_read_lock();
            break;
        case STK_CMD_READ_SIGNATURE_HVSP:
            stk500v2_hvsp_read_signature();
            break;
        case STK_CMD_READ_OSCCAL_HVSP:
            stk500v2_hvsp_read_osc_cal();
            break;

        // HVPP Commands
        case STK_CMD_ENTER_PROGMODE_PP:
            stk500v2_hvpp_enter_prog_mode();
            // SMoCommand::ShareSerialPins(true);
            break;
        case STK_CMD_LEAVE_PROGMODE_PP:
            stk500v2_hvpp_leave_prog_mode();
            // SMoCommand::ShareSerialPins(false);
            break;
        case STK_CMD_CHIP_ERASE_PP:
            stk500v2_hvpp_chip_erase();
            break;
        case STK_CMD_PROGRAM_FLASH_PP:
            stk500v2_hvpp_program_flash();
            break;
        case STK_CMD_READ_FLASH_PP:
            stk500v2_hvpp_read_flash();
            break;
        case STK_CMD_PROGRAM_EEPROM_PP:
            stk500v2_hvpp_program_eeprom();
            break;
        case STK_CMD_READ_EEPROM_PP:
            stk500v2_hvpp_read_eeprom();
            break;
        case STK_CMD_PROGRAM_FUSE_PP:
            stk500v2_hvpp_program_fuse();
            break;
        case STK_CMD_READ_FUSE_PP:
            stk500v2_hvpp_read_fuse();
            break;
        case STK_CMD_PROGRAM_LOCK_PP:
            stk500v2_hvpp_program_lock();
            break;
        case STK_CMD_READ_LOCK_PP:
            stk500v2_hvpp_read_lock();
            break;
        case STK_CMD_READ_SIGNATURE_PP:
            stk500v2_hvpp_read_signature();
            break;
        case STK_CMD_READ_OSCCAL_PP:
            stk500v2_hvpp_read_osc_cal();
            break;  

        // XPROG Commands
        case STK_CMD_XPROG_SETMODE:
            // SMoGeneral::SetXPROGMode();
            xprog_mode = XPRG_MODE_TPI;
            break;
        case STK_CMD_XPROG:
            //switch (SMoGeneral::gXPROGMode) {
            switch (xprog_mode) {
            case XPRG_MODE_TPI:
                //switch (SMoCommand::gBody[1]) {
                switch (stk_msg[1]) {
                case XPRG_CMD_ENTER_PROGMODE:
                    stk500v2_tpi_enter_prog_mode();
                    break;
                case XPRG_CMD_LEAVE_PROGMODE:
                    stk500v2_tpi_leave_prog_mode();
                    break;
                case XPRG_CMD_ERASE:
                    stk500v2_tpi_erase();
                    break;
                case XPRG_CMD_WRITE_MEM:
                    stk500v2_tpi_write_mem();
                    break;
                case XPRG_CMD_READ_MEM:
                    stk500v2_tpi_read_mem();
                    break;
                case XPRG_CMD_SET_PARAM:
                    stk500v2_tpi_set_param();
                    break;
                default:
                    goto unknow;
                }
                break;
            default:
                goto unknow;
            }
            break;
            // Pseudocommands
            /*case SMoCommand::kHeaderError:
            case SMoCommand::kChecksumError:
            case SMoCommand::kIncomplete:*/
            break;  // Ignore
        default:
        unknow:
            stk_msg[6] = STK_STATUS_CMD_FAILED;
            break;
    }
}
