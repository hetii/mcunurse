#include "general.h"
#include "cdcacm.h"
#include "usbuart.h"
#include "ssd1306.h"
#include "ssd1306_fonts.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/cm3/scs.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/stm32/adc.h>
// #include <libopencm3/stm32/iwdg.h>

uint8_t running_status;

uint16_t led_idle_run = GPIO8;
uint16_t srst_pin = SRST_PIN_V1;

int platform_hwversion(void)
{
    return 0;
}

void platform_init(void)
{
    stm_hardware_init();

    ssd1306_init();  
    ssd1306_set_cursor(2, 0);
    ssd1306_write_string(" MCU Nurse ", Font_11x18, White);
    ssd1306_set_cursor(2, 18);
    ssd1306_write_string("version 1.0", Font_11x18, White);
    ssd1306_set_cursor(2, 18+18);
    ssd1306_write_string("Created by", Font_7x10, White);
    ssd1306_set_cursor(2, 18+18+10);
    ssd1306_write_string("Grzegorz Hetman", Font_7x10, White);
    ssd1306_update_screen();

    SCS_DEMCR |= SCS_DEMCR_VC_MON_EN;

    #ifdef ENABLE_DEBUG
        void initialise_monitor_handles(void);
        initialise_monitor_handles();
    #endif

    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    /* Setup GPIO ports */
    gpio_set_mode(TMS_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_INPUT_FLOAT, TMS_PIN);
    gpio_set_mode(TCK_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, TCK_PIN);
    gpio_set_mode(TDI_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, TDI_PIN);

    platform_srst_set_val(false);

    gpio_set_mode(LED_PORT, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, led_idle_run);

    // Relocate interrupt vector table here
    extern int vector_table;
    
    SCB_VTOR = (uint32_t)&vector_table;

    platform_timing_init();

    cdcacm_init();

    // Don't enable UART if we're being debugged.
    if (!(SCS_DEMCR & SCS_DEMCR_TRCENA)){
        usbuart_init();
    }

    ssd1306_fill(Black);
    ssd1306_set_cursor(2, 0);
    ssd1306_write_string("Init done", Font_11x18, White);

    #ifdef ENABLE_DEBUG
    ssd1306_set_cursor(2, 18);
    ssd1306_write_string("Debug on!", Font_11x18, White);
    #else
    ssd1306_set_cursor(2, 18);
    ssd1306_write_string("Debug off!", Font_11x18, White);
    //    #warning Co jest do wuja?
    #endif
    
    ssd1306_update_screen();
    
    for (int i = 0; i < 19900000; i++){
            __asm__("nop");
    }

    for (int i = 0; i < 4; ++i)
    {
        for (int i = 0; i < 10000000; i++){
            __asm__("nop");
        }
        ssd1306_off();
        for (int i = 0; i < 10000000; i++){
            __asm__("nop");
        }   
        ssd1306_on();
    }
    ssd1306_off();
    
    //ssd1306_fill(Black);
    //ssd1306_update_screen();

    while (1){
    /*    DEBUG("Hello -->\n");
        for (int i = 0; i < 9900000; i++){
        __asm__("nop");
        }*/
    }
}

void platform_srst_set_val(bool assert)
{
    if (assert) {
        gpio_set_mode(SRST_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_OPENDRAIN, srst_pin);
        gpio_clear(SRST_PORT, srst_pin);
        while (gpio_get(SRST_PORT, srst_pin)) {};
    } else {
        gpio_set_mode(SRST_PORT, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, srst_pin);
        gpio_set(SRST_PORT, srst_pin);
        while (!gpio_get(SRST_PORT, srst_pin)) {};
    }
}

bool platform_srst_get_val()
{
    return gpio_get(SRST_PORT, srst_pin) == 0;
}

const char *platform_target_voltage(void)
{
    return "unknown";
}
