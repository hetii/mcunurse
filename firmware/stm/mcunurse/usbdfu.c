#include <string.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/scb.h>

#include "usbdfu.h"
#include "general.h"
#include "platform.h"
//#include "ssd1306.h"

uint32_t app_address = 0x08002000;

static bool stlink_test_nrst(void)
{
    uint16_t nrst;
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO12);
    gpio_set(GPIOB, GPIO12);
 
    for (int i = 0; i < 10000; i++){
        nrst = gpio_get(GPIOB, GPIO12);
    }
    return (nrst) ? true : false;
}

int main(void)
{
    stm_hardware_init();
  //  ssd1306_init();
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);

    if(((GPIOA_CRL & 0x40) == 0x40) && stlink_test_nrst()){
        dfu_jump_app_if_valid();
    }

    dfu_protect(DFU_MODE);
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
    
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);
    systick_set_reload(900000);

    systick_interrupt_enable();
    systick_counter_enable();

    dfu_init(&st_usbfs_v1_usb_driver, DFU_MODE);
    dfu_main();
}

void dfu_detach(void)
{
    scb_reset_system();
}

void dfu_event(void)
{
}

void sys_tick_handler(void)
{
    gpio_toggle(GPIOC, GPIO13);
}
